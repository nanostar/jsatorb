# JSatorb

JSatorb - User version - v1.0

Tool dedicated to preliminary Space Mission Analysis
(Orbit definition, surface coverage, visibility windows, eclipse calculation, etc.)

Note that this is the *user* repository. 
A *developper* repository is accessible [here](https://gitlab.isae-supaero.fr/jsatorb-dev).

# User Installation Procedure

## Pre-requisites

- Operating system: `Linux Ubuntu 22.04 LTS 64-bit` environment.
- docker and docker-compose have to be available in the user environment.
- The VTS/Prestoplot additional libraries have been installed by ISAE Administrators (see the "VTS additional libraries" paragraph below)

## Operating System and Tools installation

To check the installed version of the operating sytem, in a terminal, enter the following command line:
```
lsb_release -a
```
It shall return `Ubuntu 22.04 LTS 64-bit`.

### 1) Install docker

To install `docker`, in a terminal, enter the following command lines:

```
sudo apt-get update
(sudo apt-get upgrade)

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```
---
### 2) Install docker-compose

To install `docker-compose`, in a terminal, enter the following command lines.

#### 2.1) Add Docker’s official GPG key:

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
#### 2.2) Add Docker compose repo

```
sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo apt-get install docker-compose
```
---
### 3) Add user to Docker group
```
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
sudo systemctl restart docker
reboot
sudo chown $USER /var/run/docker.sock
```
---
### 4) Check docker installation

```
sudo docker run hello-world
```

### 5) VTS additional libraries

To check if those additional libraries/packages are available, run the following commands:
```
apt-cache show libjpeg62
apt-cache show libjpeg62:i386
apt-cache show libpng12-0
apt-cache show gcc-multilib
apt-cache show libx11-6:i386
```

__If one of those packages is missing, you have to:__

- Either call an ISAE Administrator (see the dev-detailed-install-procedure.md, or look into the shortened installation instructions below),
- Or if you are familiar with installing software through the apt tool, you can install the needed packages yourself:

    Install the needed packages:
    ```
    sudo apt update
    sudo apt install build-essential zlib1g-dev
    sudo apt install libjpeg62
    sudo apt-get install libjpeg62:i386
    sudo apt install gcc-multilib
    sudo apt install libx11-6:i386

    cd
    mkdir src
    wget https://ppa.launchpadcontent.net/linuxuprising/libpng12/ubuntu/pool/main/libp/libpng/libpng_1.2.54.orig.tar.xz
    tar Jxfv libpng_1.2.54.orig.tar.xz
    cd libpng-1.2.54
    ./configure
    make
    sudo make install
    sudo ln -s /usr/local/lib/libpng12.so.0.54.0 /usr/lib/libpng12.so
    sudo ln -s /usr/local/lib/libpng12.so.0.54.0 /usr/lib/libpng12.so.0
    ````

## Installation Procedure

The procedure to install `JSatOrb` in the user environment is described below.

### 1) Download and extraction

1) Download JSatOrb user installation archive called `JSatOrb_latest.tar.gz` from [Package Registry menu](https://gitlab.isae-supaero.fr/nanostar/jsatorb/-/packages) and unzip the archive in __the user home directory__ (i.e. /home/[username]), with the following command:

```
tar -xvzf JSatOrb_latest.tar.gz -C /home/$USER/
```

2) Go into the `JSatOrb` folder of the uncompressed archive,

```
cd ~/JSatOrb/
```

3) Launch the `jsatorb-load-docker-images.bash` script in a terminal. It installs the JSatOrb Docker images in the local repository.

```
bash jsatorb-load-docker-images.bash
```

At this point, three Docker images are ready to be deployed... but wait a little before launching them!

---

### 2) IMPORTANT: Configuring Firefox to associate JSatOrb Agent with VZ files

---

**ATTENTION**

__Learn about the following information before launching JSatorb's GUI.__  

The first time the user will ask JSatOrb to produce a compressed file to be open in VTS, Firefox will not recognize the downloaded JSatOrb file format and will show the "Save or Open With..." popup dialog.

It is important to follow correctly the process described below on the first occurence of this case, as it may be more difficult to correct the Firefox configuratin afterward.

__This process has to be done only once and for all.__


When the Firefox "Save or Open With..." popup dialog opens the first time you want to displey JSatOrb data into the VTS software:
- select "Software" in the "Open with..." part of the dialog,
- check the "always perform this action for this file type",
- confirm.

![Save or Open with Firefox dialog](doc/images/Firefox-save_or_openwith.png)  

Firefox will open the VZ file with the Software program in a separate window, which is not what we want, but now, we have to do the following steps:
- Close the "Software" window,
- Open the Firefox preferences (Main menu>Preferences),
- Go to the applications list (of the General tab),
- An entry should contain: 
    - content type: application/vnd+cssi.vtsproject+zip type
    - Action: Use Software
- Edit the asssociated action,
- Browse to your [home]/JSatOrb/JSatOrbAgent folder,
- Select the jsatorb-agent executable,
- Confirm.

![Firefox application preferences](doc/images/Firefox-preferences.png)  

__Now, VZ files produced by JSatOrb will be open by the JSatOrb Agent and forwarded to VTS.__

---
## Launching JSatOrb

Now, to start JSatOrb, the user only has to:

3.1) Run the `jsatorb-start.bash` script in a terminal :

```
cd ~/JSatOrb
bash jsatorb-start.bash
```

3.2) Open a Web browser and navigate to the URL: [http://localhost](http://localhost).

If it's not launching, hight probability is an incorrect docker right setting, see below.

3.3) The JSatOrb GUI should now displays in the navigator.

__Remark: The mission data sets saved by the user thanks to the JSatOrb GUI are store in the ```/home/[username]/JSatOrb/mission-data/``` folder.__

## Usage

At this time, a browser should be running (as well as 3 docker container)

To use JSatOrb: 

1) In the browser at address http://localhost/, enter Time Settings (top left) for TLE in `Time Settings` box by selecting a `Starting date`, an `Ending Date` (for example, choose 8600 seconds between start and end datetime) and a `Step Time` of 60 seconds.

2) Click on `import celestrak TLE` (top right). 
3) In filter, write "iridium" and pick the first satellite appearing (check box activated)
4) Click on `Validate TLE Selection`, the selected satellite should appear in `Satellites list` on left menu.
5) Click on `<` top right (going back to Cesium viewer)
6) Click on `Run mission Analysis`
7) Interact with Cesium viewer (hold right click to turn around, hold left click to zoom in/out, accelerate or hold simulation, move in time...)

From this point, you can modify name of the satellite, their TLE, add more satellites, define your own satellite. - little pen on satellite Name, in TLE list.
Be sure to stay in TLE mode (there is not yet inter-compatibility with Keplerian mode, even at the end, data is the same !)

enjoy !

## Stop JSatOrb

This command line will stop in a clean way the docker containers:
```
bash jsatorb-stop.bash
```


## Troubleshooting

If nothing happens when trying to visualize JSatOrb data with the VTS software, here are some tips in order to overcome the issue.

### JSatOrb Agent's logs

The JSatOrb Agent logs can provide useful information when the VTS visualization doesn't work.
The logs are to be found in __`/home/[username]/JSatOrb/JSatOrbAgent/jsatorb-agent.log`__.

A possible issue is that the VTS additional libraries have not been installed in your environment, with a similar message as below:
```
error while loading shared libraries: libpng12.so.0: cannot open shared object file: No such file or directory
RETURN CODE 0
```

If it's the case, see the paragraph below to resolve this issue.

### HotFix for frontend: 

Because
http://celestrak.org/NORAD/elements/gp.php?GROUP=active&FORMAT=tle 
changed API

```
docker stop jsatorb-frontend-container
docker rm jsatorb-frontend-container

docker run -p 80:80 -d --name jsatorb-frontend-fix-container gitlab-registry.isae-supaero.fr/jsatorb-dev/jsatorb-frontend:hotFix 
```

### HotFix for frontend alternative

```
docker build  . -t jsatorb-frontend:hotFix

#test
docker build  . -t jsatorb-frontend:hotFix
docker run -p 80:80 -it --rm jsatorb-frontend-luplink:hotfix
```


### VTS LIBGL issue (Docker graphics)

```
LIBGL_ALWAYS_SOFTWARE=1 ./startVTS
```

### Prestoplot Troubleshoot

```
export LANG=fr_FR.UTF-8 
```

### Scilab 6.0.2 Troubleshoot


https://forum.ubuntu-fr.org/viewtopic.php?id=2051849

```
LIBGL_ALWAYS_SOFTWARE=1 ./scilab

--> exec('/home/$USER/Utils/celestlabx/loader.sce', -1)
```

